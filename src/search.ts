import { fuzzyContains } from "./strings";

export interface IRawFileMatch {
    base?: string;
    /**
     * The path of the file relative to the containing `base` folder.
     * This path is exactly as it appears on the filesystem.
     */
    relativePath: string;
    /**
     * This path is transformed for search purposes. For example, this could be
     * the `relativePath` with the workspace folder name prepended. This way the
     * search algorithm would also match against the name of the containing folder.
     *
     * If not given, the search algorithm should use `relativePath`.
     */
    searchPath: string | undefined;
}

export function isFilePatternMatch(candidate: IRawFileMatch, normalizedFilePatternLowercase: string): boolean {
    const pathToMatch = candidate.searchPath ? candidate.searchPath : candidate.relativePath;
    return fuzzyContains(pathToMatch, normalizedFilePatternLowercase);
}