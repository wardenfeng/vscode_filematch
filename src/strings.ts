/**
 * Checks if the characters of the provided query string are included in the
 * target string. The characters do not have to be contiguous within the string.
 */
export function fuzzyContains(target: string, query: string): boolean {
    if (!target || !query) {
        return false; // return early if target or query are undefined
    }

    if (target.length < query.length) {
        return false; // impossible for query to be contained in target
    }

    const queryLen = query.length;
    const targetLower = target.toLowerCase();

    let index = 0;
    let lastIndexOf = -1;
    while (index < queryLen) {
        const indexOf = targetLower.indexOf(query[index], lastIndexOf + 1);
        if (indexOf < 0) {
            return false;
        }

        lastIndexOf = indexOf;

        index++;
    }

    return true;
}