import { IRawFileMatch, isFilePatternMatch } from "./search";
import * as path from './path';


// The ParsedExpression returns a Promise iff hasSibling returns a Promise.
type ParsedExpression = (path: string, basename?: string, hasSibling?: (name: string) => boolean | Promise<boolean>) => string | null | Promise<string | null> /* the matching pattern */;

class FileWalker {
    private maxResults: number | null;
    private exists: boolean;
    private isLimitHit: boolean;
    private resultCount: number;
    private includePattern: ParsedExpression | undefined;
    private filePattern: string;
    private normalizedFilePatternLowercase: string | null = null;

    private matchFile(onResult: (result: IRawFileMatch) => void, candidate: IRawFileMatch): void {
        if (this.isFileMatch(candidate) && (!this.includePattern || this.includePattern(candidate.relativePath, path.basename(candidate.relativePath)))) {
            this.resultCount++;

            if (this.exists || (this.maxResults && this.resultCount > this.maxResults)) {
                this.isLimitHit = true;
            }

            if (!this.isLimitHit) {
                onResult(candidate);
            }
        }
    }

    private isFileMatch(candidate: IRawFileMatch): boolean {
        // Check for search pattern
        if (this.filePattern) {
            if (this.filePattern === '*') {
                return true; // support the all-matching wildcard
            }

            if (this.normalizedFilePatternLowercase) {
                return isFilePatternMatch(candidate, this.normalizedFilePatternLowercase);
            }
        }

        // No patterns means we match all
        return true;
    }
}